package E2E.TestFramework;

import static org.testng.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import org.testng.annotations.Test;
import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;
import org.json.JSONObject;
import com.google.gson.Gson;

import POJO.AddUserPOJO;
import POJO.AddUserResponsePOJO;
import POJO.Book;
import POJO.Books;
import POJO.SerializeToFilePOJO;
import Utility.DeserializeFromFile;
import Utility.DeserializeFromFile;
import Utility.SerializeToFileUtility;

import static io.restassured.RestAssured.*;

public class AppTest {

	@Test(priority=1)
    public void serializeToFile() {
		try {		
			System.out.println("Test1");
			SerializeToFilePOJO obj = new SerializeToFilePOJO(30, 10);
			SerializeToFileUtility seFile = new SerializeToFileUtility();
			seFile.SerializeToFile(obj, "serializedFile");
			System.out.println("===============================================");
		} catch (Exception e) {			
			e.printStackTrace();
		} 
	}
	
	@Test(priority=2)
    public void deSerializeToFile() {
		try {				
			System.out.println("Test2");
			SerializeToFilePOJO obj = new SerializeToFilePOJO(25, 12);
			SerializeToFileUtility seFile = new SerializeToFileUtility();
			seFile.SerializeToFile(obj, "serializedFile");
			
			DeserializeFromFile deFile = new DeserializeFromFile();
			SerializeToFilePOJO deSerialized = (SerializeToFilePOJO) deFile.deSerializeFromFileToObject("serializedFile");
			System.out.println("Height is: "+deSerialized.getHeight());
			System.out.println("Width is: "+deSerialized.getWidth());
			System.out.println("===============================================");
		} catch (Exception e) {			
			e.printStackTrace();
		} 
	}
	
	// Basic authentication using user name and password
	@Test(priority = 3)
	public void basicAuth() {
		System.out.println("Test3");
		RequestSpecification httpRequest = RestAssured.given().auth().basic("postman", "password");
		Response res = httpRequest.get("https://postman-echo.com/basic-auth");
		ResponseBody body = res.body();
		// Converting the response body to string
		String rbdy = body.asString();
		System.out.println("Data from the GET API- " + rbdy);
		System.out.println("Reponse Status Line- " + res.getStatusLine());
		System.out.println("Reponse Status Line- " + res.getStatusCode());
		System.out.println("===============================================");
	}
    
    //Preemptive basic authentication using user name and password
	@Test(priority=4)
    public void preemptiveBasicAuth() {
    	try {
    		System.out.println("Test4");
    		 //Using the preemptive directive of basic auth to send credentials to the server
            RequestSpecification httpRequest = RestAssured.given().auth().preemptive().basic("postman", "password");
            Response res = httpRequest.get("https://postman-echo.com/basic-auth");
            ResponseBody body = res.body();
            //Converting the response body to string
            String rbdy = body.asString();
            System.out.println("Data from the GET API- "+body);
            System.out.println("Reponse Status Line- "+res.getStatusLine());
            System.out.println("Reponse Status Line- "+res.getStatusCode());
            System.out.println("===============================================");
    	}catch(Exception e) {
    		System.out.println("Exception: "+e);
    	}       
    }
    
    //we provided incorrect password, so it will fail
	@Test(priority=5)
    public void preemptiveBasicAuthNegative() {
    	try {
    		System.out.println("Test5");
    		 //Using the preemptive directive of basic auth to send credentials to the server
            RequestSpecification httpRequest = RestAssured.given().auth().preemptive().basic("postman", "password1");
            Response res = httpRequest.get("https://postman-echo.com/basic-auth");
            ResponseBody body = res.body();
            //Converting the response body to string
            String rbdy = body.asString();
            System.out.println("Data from the GET API- "+body);
            System.out.println("Reponse Status Line- "+res.getStatusLine());
            System.out.println("Reponse Status Line- "+res.getStatusCode());
            System.out.println("===============================================");
    	}catch(Exception e) {
    		System.out.println("Exception: "+e);
    	}       
    }
    
    //we provided incorrect base URL, so it will fail
	@Test(priority=6)
    public void exceptionDueToEndIncorrectPoint() {
    	try {
    		System.out.println("Test6");
    		 //Using the preemptive directive of basic auth to send credentials to the server
            RequestSpecification httpRequest = RestAssured.given().auth().preemptive().basic("postman", "password");
            Response res = httpRequest.get("https://postman-echo.com/basic-auth1");
            ResponseBody body = res.getBody();
            //Converting the response body to string
            String rbdy = body.asString();
            System.out.println("Data from the GET API- "+body);
            System.out.println("Reponse Status Line- "+res.getStatusLine());
            System.out.println("Reponse Status Line- "+res.getStatusCode());
            System.out.println("===============================================");
    	}catch(Exception e) {
    		System.out.println("Exception: "+e);
    	}       
    }
    
    //get user details based on user number passed
	@Test(priority=7)
    public void testGetRequest() {	
		System.out.println("Test7");
		// Specify the base URL to the RESTful web service
		RestAssured.baseURI = "https://reqres.in/api/users/2";         
		// Get the RequestSpecification of the request to be sent to the server. 
		RequestSpecification httpRequest = RestAssured.given(); 
		// specify the method type (GET) and the parameters if any. 
		//In this case the request does not take any parameters 
		//Response response = httpRequest.request(Method.GET, "");
		Response response = httpRequest.get();
		// Print the status and message body of the response received from the server 
		System.out.println("Status received => " + response.getStatusLine());
		System.out.println("Status code received => " + response.getStatusCode());
		System.out.println("Response=>" + response.prettyPrint());
		System.out.println("===============================================");
    }   
	
	//Convert JSON request to POJO(JAVA Object)
	@Test(priority=8)
	public void convertJSONToPOJO() {		
		System.out.println("Test8");
	    Response response;
	    String jsonString;
	    String id;
	    AddUserPOJO authRequest = new AddUserPOJO("Yuvin","2019");
	    System.out.println(authRequest.name);
	    System.out.println(authRequest.job);
		RestAssured.baseURI = "https://reqres.in/";		
        RequestSpecification request = RestAssured.given();

        request.header("Content-Type", "application/json");
        response = request.body(authRequest).post("/api/users");

        jsonString = response.getBody().asString();

        System.out.println(jsonString);
        System.out.println(response.getStatusCode());
        System.out.println(response.getStatusLine());
        id = JsonPath.from(jsonString).get("id");
        System.out.println("ID is: "+id);
        System.out.println("===============================================");
	}
	
	//Convert JSON response to POJO (JAVA Object)
	@Test(priority=9)
	public void convertJSONResponseToPOJO() {		
		try {
			System.out.println("Test9");
			Response response;
		    String jsonString;
		    String id;
		    AddUserPOJO authRequest = new AddUserPOJO("Yuvin","2019");
		    System.out.println(authRequest.name);
		    System.out.println(authRequest.job);
			RestAssured.baseURI = "https://reqres.in/";		
	        RequestSpecification request = RestAssured.given();

	        request.header("Content-Type", "application/json");
	        response = request.body(authRequest).post("/api/users");

	        jsonString = response.getBody().asString();
	        AddUserResponsePOJO addedUserResponse = response.getBody().as(AddUserResponsePOJO.class);        
	        System.out.println(addedUserResponse.getName()+" | "+addedUserResponse.getJob()+" | "+addedUserResponse.getId()+" | "+addedUserResponse.getCreatedAt());
	        System.out.println("===============================================");
		}catch(Exception e) {
			System.out.println("Exception is: "+e.toString());
		}
	}
	
	//Convert JSON response to nested POJO (JAVA Object)
	@Test(priority=10)
	public void listOfBooksAreAvailable() {
		System.out.println("Test10");
		RestAssured.baseURI = "https://bookstore.toolsqa.com";
		RequestSpecification request = RestAssured.given();
		
		Response response = request.get("/BookStore/v1/Books");

		// Deserializing the Response body into Books class 
		Books books = response.getBody().as(Books.class); 
		for(Book book : books.books) {
			System.out.println("ISBN: "+book.isbn);
			System.out.println("Pages: "+book.pages);
			System.out.println("Website: "+book.website);
			System.out.println("=========================================");
		}		
	}
}