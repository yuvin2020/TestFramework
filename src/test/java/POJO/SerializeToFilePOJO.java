package POJO;

import java.io.Serializable;

public class SerializeToFilePOJO implements Serializable {

	private double height;
	private double width;
	
	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public SerializeToFilePOJO(double height, double width)
	{
		this.height = height;
		this.width = width;
	}
}